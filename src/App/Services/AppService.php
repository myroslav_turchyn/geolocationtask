<?php

namespace App\Services;

use App\Models\UserLocation;
use Pimple\Container;
use Symfony\Component\Config\Definition\Exception\Exception;

class AppService
{

    /**
     * @var Container
    */
    private $app;


    public function __construct(Container $app)
    {
        $this->app = $app;
    }

    /** Get location
     *
     * @param $ip
     *
     * @return array|null
     * <pre>
     *  [
     *    'city' => (string) Client city.
     *    'country' => (string) Client country.
     *  ]
     * </pre>
     */
    public function getLocation($ip)
    {
        if (!$location = UserLocation::find_by_ip($ip)) {
            if ($location = $this->getLocationFromIpInfo($ip)) {
                $this->saveLocationToDB($location);
            }
        }
        if($location){
            return  ['ip' => $location->ip, 'city' => $location->city, 'country' => $location->country];
        }

        return null;
    }


    /**
     * Get location from ip info
     *
     * @param string $ip
     *
     * @return object|null
     * <pre>
     *  [
     *    'ip' => (string) Client ip.
     *    'city' => (string) Client city.
     *    'region' => (string) Client region.
     *    'country' => (string) Client country.
     *    'loc' => (string) Client loc.
     *    'postal' => (string) Client postal code.
     *  ]
     * </pre>
     */
    public function getLocationFromIpInfo($ip)
    {
        $details = json_decode(file_get_contents("{$this->app['ip_info_url']}/{$ip}/json"));
        return $details;
    }


    /**
     * Save location to db
     *
     * @param object $location
     *
     * @return boolean
     */
    public function saveLocationToDB($location)
    {
        $newUserLocation = new UserLocation();
        $newUserLocation->assign_attribute('ip', $location->ip);
        $newUserLocation->assign_attribute('city', $location->city);
        $newUserLocation->assign_attribute('country', $location->country);

        return $newUserLocation->save();

    }

}
