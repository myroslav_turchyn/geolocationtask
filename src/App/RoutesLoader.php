<?php

namespace App;

use App\Controllers\AppController;
use Pimple\Container;

class RoutesLoader
{
    private $app;

    public function __construct(Container $app)
    {
        $this->app = $app;
        $this->instantiateControllers();

    }

    private function instantiateControllers()
    {
        $this->app['app.controller'] = function() {
            return new AppController($this->app);
        };
    }

    public function bindRoutesToControllers()
    {
        $api = $this->app["controllers_factory"];

        $api->get('/', "app.controller:index");
        $api->get('/location/', "app.controller:getLocation");

        $this->app->mount("/",$api);
    }
}

