<?php

namespace App\Models;

use ActiveRecord\Model;

/**
 * This is the model class for table "{{user_location}}".
 *
 * @property int $id
 * @property string $ip
 * @property string $city
 * @property string $country
 *
 */
class UserLocation extends Model
{

    /**
     * @var string
     */
    static $table_name = 'user_location';

    /**
     * @var array
     */
    static $validates_format_of = [
        ['ip', 'with' => '/^(?=.*[^\.]$)((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.?){4}$/']
    ];

    /**
     * @var array
     */
    static $validates_uniqueness_of = [
        ['ip']
    ];

    /**
     * @var array
     */
    static $validates_presence_of = [
        ['ip','city','country']
    ];

    /**
     * @var array
     */
    static $validates_length_of = [
        ['city', 'within' => [1,50]],
        ['country', 'within' => [1,50]]
    ];

}
