<?php

namespace App;

use App\Services\AppService;
use Pimple\Container;

class ServicesLoader
{
    protected $app;

    public function __construct(Container $app)
    {
        $this->app = $app;
    }

    public function bindServicesIntoContainer()
    {
        $this->app['app.service'] = function() {
            return new AppService($this->app);
        };
    }
}

