<?php

namespace App\Controllers;

use Pimple\Container;
use Silex\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AppController
{

    /**
     * @var Container
     */
    protected $app;


    public function __construct(Container $app)
    {
        $this->app = $app;
    }

    /**
     * Index
     * Action for home page
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        return $this->app['twig']->render('index.html.twig', ['user_ip' => $request->server->get('REMOTE_ADDR')]);
    }


    /**
     * Get location
     * Action for getting client's current location data
     *
     * @param Request $request
     *
     * @return JsonResponse
     * <pre>
     *    {
     *       'city' => (string) Client city.
     *       'country' => (string) Client country.
     *    }
     * </pre>
     */
    public function getLocation(Request $request)
    {
        $locationData = $this->app['app.service']->getLocation($request->server->get('REMOTE_ADDR'));

        return new JsonResponse($locationData);
    }

}
