<?php

use Silex\Application;
use Silex\Provider\AssetServiceProvider;
use Silex\Provider\TwigServiceProvider;
use Silex\Provider\ServiceControllerServiceProvider;
use Silex\Provider\HttpFragmentServiceProvider;
use Symfony\Component\HttpFoundation\Request;
use App\Providers\ActiveRecordServiceProvider;
use App\ServicesLoader;
use App\RoutesLoader;

$app = new Application();
$app->register(new AssetServiceProvider());
$app->register(new TwigServiceProvider());
$app->register(new HttpFragmentServiceProvider());
$app->register(new ServiceControllerServiceProvider());
$app->register(new ActiveRecordServiceProvider(array(
        'ar.model_path'   => __DIR__.'/App//Models',
        'ar.default_connection' => 'development',
        'ar.connections' => array(
            'development' => 'mysql://test_user:secret@localhost/test'
        ),
    ))
);

$servicesLoader = new ServicesLoader($app);
$servicesLoader->bindServicesIntoContainer();

$routesLoader = new RoutesLoader($app);
$routesLoader->bindRoutesToControllers();

$app->before(function (Request $request) {
    if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
        $data = json_decode($request->getContent(), true);
        $request->request->replace(is_array($data) ? $data : array());
    }
});

return $app;
