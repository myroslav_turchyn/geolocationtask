var UserLocation = function () {
    var userCountryEl = $('#userCountry'),
        userCityEl = $('#userCity'),
        locationUrl = '/location/',
        self = this;

    this.init = function () {
        $("#btnGetGeoLocation").unbind("click").bind("click", function (e) {
            self.getGeoLocation();
        });
    };

    this.getGeoLocation = function() {
        $.get(locationUrl, function (data, status) {
            self.setUserPlaceInfo(data);
        });
    }

    this.setUserPlaceInfo = function (data) {
        userCountryEl.text(data.country ? data.country : '');
        userCityEl.text(data.city ? data.city : '');
    }
}

var userLocation = new UserLocation();
userLocation.init();



